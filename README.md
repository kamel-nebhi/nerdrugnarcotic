This project is in related to this git repo: https://gitlab.com/kamel-nebhi/sfcrime-analysis

* Gazetteer source:
	* https://en.wikipedia.org/wiki/List_of_Schedule_I_drugs_(US)
	* https://en.wikipedia.org/wiki/List_of_Schedule_II_drugs_(US)
	* https://en.wikipedia.org/wiki/List_of_Schedule_III_drugs_(US)
	* https://en.wikipedia.org/wiki/List_of_Schedule_IV_drugs_(US)
    * https://en.wikipedia.org/wiki/List_of_Schedule_V_drugs_(US)